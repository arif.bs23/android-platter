package com.example.androidplatter.auth.repo

import com.example.androidplatter.auth.repo.model.LoginResponse

interface AuthRemoteDS {

    suspend fun loginByEmail(email: String, password: String): LoginResponse

}
