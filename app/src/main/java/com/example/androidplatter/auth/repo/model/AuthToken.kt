package com.example.androidplatter.auth.repo.model

import com.google.gson.annotations.SerializedName

data class AuthToken(
    @SerializedName("token_type") val tokenType: String? = null,
    @SerializedName("expires_in") val expiresIn: Long? = null,
    @SerializedName("access_token") val accessToken: String? = null,
    @SerializedName("refresh_token") val refreshToken: String? = null,
)
