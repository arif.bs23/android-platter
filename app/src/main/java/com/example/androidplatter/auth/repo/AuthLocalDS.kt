package com.example.androidplatter.auth.repo

import com.example.androidplatter.auth.repo.model.AuthToken

interface AuthLocalDS {

    suspend fun saveEmail(email: String)

    suspend fun saveAuthToken(authToken: AuthToken)

}

