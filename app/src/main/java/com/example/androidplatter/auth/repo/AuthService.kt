package com.example.androidplatter.auth.repo

import com.example.androidplatter.auth.repo.model.EmailLoginRequest
import com.example.androidplatter.auth.repo.model.LoginResponse
import com.example.androidplatter.auth.repo.model.RefreshTokenResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST("/login")
    suspend fun loginByEmail(@Body request: EmailLoginRequest): Response<LoginResponse>

    @POST("/refresh-token")
    fun refreshToken(): Call<Response<RefreshTokenResponse>>
}
