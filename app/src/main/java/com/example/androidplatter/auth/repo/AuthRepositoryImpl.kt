package com.example.androidplatter.auth.repo

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class AuthRepositoryImpl private constructor(
    private val localDS: AuthLocalDS,
    private val remoteDS: AuthRemoteDS
) : AuthRepository {

    companion object {

        @Volatile
        private var instance: AuthRepositoryImpl? = null

        fun getInstance(localDS: AuthLocalDS, remoteDS: AuthRemoteDS): AuthRepositoryImpl {
            return instance ?: synchronized(AuthRepositoryImpl::class) {
                return@synchronized instance ?: AuthRepositoryImpl(localDS, remoteDS).apply {
                    instance = this
                }
            }
        }
    }

    override fun loginByEmail(email: String, password: String): Flow<Unit> {
        return flow {

            val response = remoteDS.loginByEmail(email, password)
            if (response.authToken != null) {
                localDS.saveEmail(email)
                localDS.saveAuthToken(response.authToken)
            }

            emit(Unit)
        }
            .flowOn(Dispatchers.IO)
    }

}
