package com.example.androidplatter.auth.repo

import kotlinx.coroutines.flow.Flow

interface AuthRepository {

    fun loginByEmail(email: String, password: String): Flow<Unit>
}
