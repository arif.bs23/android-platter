package com.example.androidplatter.auth.repo

import com.example.androidplatter.auth.repo.model.AuthToken
import com.example.androidplatter.common.preference.UserPreference

class AuthLocalDSImpl private constructor(
    private val userPreference: UserPreference
) : AuthLocalDS {

    companion object {

        @Volatile
        private var instance: AuthLocalDSImpl? = null

        fun getInstance(userPreference: UserPreference): AuthLocalDSImpl {
            return instance ?: synchronized(AuthLocalDSImpl::class) {
                return@synchronized instance ?: AuthLocalDSImpl(userPreference).apply {
                    instance = this
                }
            }
        }
    }

    override suspend fun saveEmail(email: String) {
        userPreference.saveEmail(email)
    }

    override suspend fun saveAuthToken(authToken: AuthToken) {
        userPreference.saveAccessToken(authToken.accessToken)
        userPreference.saveRefreshToken(authToken.refreshToken)
    }
}
