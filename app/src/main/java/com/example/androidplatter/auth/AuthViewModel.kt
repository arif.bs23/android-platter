package com.example.androidplatter.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.androidplatter.auth.repo.AuthRepository
import com.example.androidplatter.common.EventLiveData
import com.example.androidplatter.common.LoaderState
import com.example.androidplatter.common.ui.BaseViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class AuthViewModel(private val authRepo: AuthRepository) : BaseViewModel() {

    private val loginSuccess = EventLiveData<Unit>()

    fun onLoginSucceed(): LiveData<Unit> = loginSuccess

    fun login(email: String, password: String) {
        blockingLoaderState.value = LoaderState.LOADING
        authRepo.loginByEmail(email, password)
            .catch { error ->
                blockingLoaderState.postValue(LoaderState.LOADED)
                val errorHandled = handleCommonError(error)
                if (!errorHandled) {
                    // TODO handle api specific error
                }
            }
            .onEach {
                blockingLoaderState.postValue(LoaderState.LOADED)
                loginSuccess.postValue(Unit)
            }
            .launchIn(viewModelScope)
    }
}
