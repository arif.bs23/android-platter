package com.example.androidplatter.auth.repo.model

import com.google.gson.annotations.SerializedName

data class RefreshTokenResponse(
    @SerializedName("status") val status: String? = null,
    @SerializedName("status_code") val statusCode: Int? = null,
    @SerializedName("message") val message: String? = null,
    @SerializedName("data") val refreshToken: AuthToken? = null,
)
