package com.example.androidplatter.auth.repo

import com.example.androidplatter.auth.repo.model.EmailLoginRequest
import com.example.androidplatter.auth.repo.model.LoginResponse
import com.example.androidplatter.common.webservice.ResponseParser

class AuthRemoteDSImpl private constructor(private val authService: AuthService) : AuthRemoteDS {

    companion object {

        @Volatile
        private var instance: AuthRemoteDSImpl? = null

        fun getInstance(authService: AuthService): AuthRemoteDSImpl {
            return instance ?: synchronized(AuthRemoteDSImpl::class) {
                return@synchronized instance ?: AuthRemoteDSImpl(authService).apply {
                    instance = this
                }
            }
        }
    }

    override suspend fun loginByEmail(email: String, password: String): LoginResponse {
        val loginRequest = EmailLoginRequest(email, password)
        val response = authService.loginByEmail(loginRequest)
        return ResponseParser.parse(response)
    }
}
