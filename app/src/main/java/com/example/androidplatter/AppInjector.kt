package com.example.androidplatter

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.example.androidplatter.auth.repo.*
import com.example.androidplatter.common.preference.*
import com.example.androidplatter.common.utils.BASE_URL
import com.example.androidplatter.common.webservice.*
import okhttp3.Authenticator
import okhttp3.Interceptor

object AppInjector {

    private fun getSettingsPreference(context: Context): SettingsPreference {
        val prefs =
            context.getSharedPreferences("${context.packageName}_preference_settings", MODE_PRIVATE)
        return SettingsPreferenceImpl.getInstance(prefs)
    }

    private fun getUserPreference(context: Context): UserPreference {
        val prefs =
            context.getSharedPreferences("${context.packageName}_preference_user", MODE_PRIVATE)
        return UserPreferenceImpl.getInstance(prefs)
    }

    fun getAppPreference(context: Context): AppPreference {
        return AppPreferenceImpl.getInstance(
            getUserPreference(context),
            getSettingsPreference(context)
        )
    }

    private fun getHttpLogger(): HttpLogger {
        return HttpLoggerImpl.getInstance()
    }

    private fun getAuthInterceptor(context: Context): Interceptor {
        return AuthInterceptor.getInstance(getUserPreference(context))
    }

    private fun getRefreshApiClient(): ApiClient {
        return RefreshApiClient.getInstance(BASE_URL, getHttpLogger().getLoggingInterceptor())
    }

    private fun getAuthenticator(context: Context): Authenticator {
        val authService = getRefreshApiClient().createService(AuthService::class.java)
        return TokenAuthenticator.getInstance(getUserPreference(context), authService, getAppCache(context))
    }

    private fun getAppCache(context: Context): AppCache {
        return AppCacheImpl.getInstance(context)
    }

    private fun getRestApiClient(context: Context): ApiClient {
        return RestApiClient.getInstance(
            BASE_URL,
            getAuthInterceptor(context),
            getAuthenticator(context),
            getAppCache(context),
            getHttpLogger().getLoggingInterceptor()
        )
    }

    fun getAuthService(context: Context): AuthService {
        return getRestApiClient(context).createService(AuthService::class.java)
    }

    fun getAuthRemoteDS(context: Context): AuthRemoteDS {
        return AuthRemoteDSImpl.getInstance(getAuthService(context))
    }

    fun getAuthLocalDS(context: Context): AuthLocalDS {
        return AuthLocalDSImpl.getInstance(getUserPreference(context))
    }

    fun getAuthRepository(context: Context): AuthRepository {
        return AuthRepositoryImpl.getInstance(getAuthLocalDS(context), getAuthRemoteDS(context))
    }
}
