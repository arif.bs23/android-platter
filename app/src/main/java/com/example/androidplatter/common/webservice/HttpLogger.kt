package com.example.androidplatter.common.webservice

import okhttp3.Interceptor

interface HttpLogger {
    fun getLoggingInterceptor(): Interceptor
}

