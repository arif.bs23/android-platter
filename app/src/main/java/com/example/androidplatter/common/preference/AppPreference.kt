package com.example.androidplatter.common.preference

interface AppPreference {

    fun getUserPreference(): UserPreference

    fun getSettingsPreference(): SettingsPreference
}
