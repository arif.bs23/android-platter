package com.example.androidplatter.common.webservice

import com.example.androidplatter.auth.repo.AuthService
import com.example.androidplatter.common.preference.UserPreference
import com.example.androidplatter.common.utils.BEARER
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenAuthenticator private constructor(
    private val preference: UserPreference,
    private val authService: AuthService,
    private val appCache: AppCache
) : Authenticator {

    companion object {

        @Volatile
        private var instance: TokenAuthenticator? = null

        fun getInstance(
            preference: UserPreference,
            authService: AuthService,
            appCache: AppCache
        ): TokenAuthenticator {
            return instance ?: synchronized(TokenAuthenticator::class) {
                return instance ?: TokenAuthenticator(preference, authService, appCache).apply {
                    instance = this
                }
            }
        }
    }

    private var refreshedAt = 0L

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(TokenAuthenticator::class.java) {

            val accessToken = preference.getAccessToken()
            if (response.request.header(AuthInterceptor.AUTHORIZATION) != "$BEARER $accessToken") {
                return response.request.newBuilder()
                    .header(AuthInterceptor.AUTHORIZATION, "$BEARER ${accessToken.orEmpty()}")
                    .build()
            }

            if (System.currentTimeMillis() - refreshedAt < 5000) {
                return response.request
            }

            val refreshToken = preference.getRefreshToken()
            if (refreshToken.isNullOrBlank()) {
                logoutCurrentUser()
                throw UnauthorizedException()
            }

            val refreshResponse = authService.refreshToken().execute()
            if (!refreshResponse.isSuccessful) {
                logoutCurrentUser()
                throw UnauthorizedException()
            }

            val tokenData = refreshResponse.body()?.body()?.refreshToken
            if (tokenData?.accessToken.isNullOrBlank()) {
                logoutCurrentUser()
                throw UnauthorizedException()
            }

            preference.saveAccessToken(tokenData?.accessToken)
            preference.saveRefreshToken(tokenData?.refreshToken)

            return response.request.newBuilder()
                .header(
                    AuthInterceptor.AUTHORIZATION,
                    "$BEARER ${tokenData?.accessToken.orEmpty()}"
                )
                .build()
        }
    }

    private fun logoutCurrentUser() {
        preference.clearUserPreferences()
        appCache.clearCache()
    }
}
