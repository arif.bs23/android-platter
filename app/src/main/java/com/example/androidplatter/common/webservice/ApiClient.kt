package com.example.androidplatter.common.webservice

interface ApiClient {

    fun <Service> createService(serviceClass: Class<Service>): Service
}
