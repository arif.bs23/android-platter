package com.example.androidplatter.common

enum class LoaderState {
    LOADING,    // started a network call
    LOADED,     // finished a network call
}
