package com.example.androidplatter.common.webservice

class ApiException(val code: Int, val error: ApiError?) : Exception(error?.message)

class UnauthorizedException(message: String? = null) : Exception(message)
