# Overview
This boilerplate contains android starter project which uses **MVVM** pattern in the UI layer, **Repository** pattern in the data layer, **coroutine** for concurrency pattern. 
Dependency injection is done manually but it can be replaced by any framework with minimal effort.
Each component class have an interface to hide its implementation details & make it testable.

This project contains a common & auth package. Common package contains base classes, utility classes & network related classes.

# Common Package
#### pagination
This package contains Pagination related common classes and functionalities. Pagination is a technique used to display large amounts of data in smaller, manageable chunks or pages. It involves dividing data into separate pages and providing navigation to move between them.

- **DiffUtilCallback** : The DiffUtilCallback class is used to calculate the difference between two lists of data in Android Jetpack's DiffUtil class. It takes two lists and two optional functions to compare items and their contents. This class is usually used to update a RecyclerView adapter with the new data.
- **PaginateAbleAdapter** : This is an abstract class for a RecyclerView adapter that supports pagination and multiple view types. It handles the display of a loading indicator, empty view, and error view, in addition to the normal data. It also provides methods to set the network and error states, reset the data, and submit the new data using the DiffUtil library.
- **PaginateAbleScrollListener** : This code defines an abstract class for a RecyclerView scroll listener that supports pagination. It checks if the adapter is loading data or has reached the end of available data and triggers the loadMoreData method when the user scrolls to the end of the list.

#### ui
This package contains common UI elements, Base classes etc.

- **BaseActivity** : This is an abstract class that serves as a base activity for other activities in an Android app. It handles common functionality such as showing toast and snack bar messages, navigating to login screen, and monitoring network connectivity. Other activities can inherit from this class and override its methods as needed.
- **BaseBottomSheetDialog** : This is an abstract class for creating bottom sheet dialogs in Android using the BottomSheetDialogFragment class. It can be extended to create custom bottom sheet dialogs.
- **BaseDialogFragment** : The BaseDialogFragment class is an abstract class that extends DialogFragment and provides a basic implementation for creating a custom dialog fragment with action button callbacks. It also sets a custom style and allows back button to work as the activity's back button.
- **BaseFragment** : The purpose of this class is to serve as a base class for fragments in an Android app. It provides common functionality such as handling back press events, observing common data from a view model, and setting the current screen for analytics purposes.
- **BaseViewHolder** : The BaseViewHolder is an abstract class used as a base class for implementing a view holder in a RecyclerView. It defines an abstract bind method that must be implemented by its subclasses to bind the view holder with a specific data item.
- **BaseViewModel** : This class is a base class for ViewModel classes. It provides common error handling and LiveData objects for common events such as loading state changes, remote message received, connection errors, and unknown errors.
- **ConnectionStateMonitor** : This class is responsible for monitoring changes in the device's network connectivity status and notifying its listener when the device loses or regains network connectivity. It uses the ConnectivityManager class to register and unregister a network callback, and implements the onAvailable and onLost methods to detect changes in network connectivity.
- **FragmentContainer** : The FragmentContainer interface defines a set of methods that a host activity must implement to provide navigation and user feedback functionalities to the fragments it contains. These methods include displaying snack and toast messages, navigating to the login screen, and defining callback actions.
- **PartiallyVisibleHorizontalLayoutManager** : This class is a custom implementation of a LinearLayoutManager for a RecyclerView in a horizontal orientation. It allows the width of the items to be resized based on a minimum percentage of the RecyclerView width.
- **SpaceItemDecoration** : This class is an implementation of the RecyclerView ItemDecoration interface, which adds spacing between items in a RecyclerView. It allows for customisation of spacing based on the type of layout manager being used (linear or grid) and the position of the item in the RecyclerView.

#### utils
This package contains a set of utility classes or functions that are reusable across different parts of an Android application.

- **BuildConfigs** : This class contains BuildConfig constant values. BuildConfig is an automatically generated class in Android that contains constants and configuration values for the current build variant.
- **Constants** : This class contains the common constant values used throughout the project.
- **DateUtils** : This class contains Date and Time related common utility functions used in the whole app.
- **UiUtils** : This class contains Ui related common utility functions.
- **Utils** : This class provides a set of extension functions for Kotlin's basic data types, such as Boolean, Int, Long, Float, Double, and String, that return default values in case of null or missing values and other utility functions for mapping and conversion.

#### Web Service
- **ApiClient** : API clients tend to provide the client for API calls, for example you may use this client with Retrofit, Volley etc. And the API Client is fully unit testable, just need to have a test Api Client implementation.
- **RestApiClient** : Implements the ApiClient interface is an abstraction that defines the behavior of a RESTful web service client. The Rest API Client takes a few params to provide api client and http client.Finally, the class implements the createService method from the ApiClient interface. The createService method creates a new instance of a REST-ful web service interface using the Retrofit instance.
- **ApiError** : Its main purpose is to provide a generic contract for any kind of api error responses.
- **AppCache** : This class provides a way to cache data from your API calls so that subsequent calls can be faster. It's especially useful if your app needs to display data that does not change very often. And have the ability to clear the cache.Auth Interceptor.
- **HttpLogger and HttpLoggerImplementation** : The purpose of this class is to provide an Interceptor that can log HTTP requests and responses for debugging purposes.This class provides a simple and efficient way to log HTTP requests and responses using the OkHttp library. It can be used in conjunction with other interceptors and authenticators to create a comprehensive API service for an Android app.Api Exception.
- **RequestException** : The ApiException class represents an exception that is thrown when an API call fails. The ApiException class extends the Exception class and passes the error message property to its constructor and it will be set to the error message provided by the ApiError object, if it exists.  The UnauthorizedException class represents an exception that is thrown when the API call fails due to authentication failure or invalid credentials.
- **ResponseParser** : Serves one of the main purposes for api service. Every api passes through this class and any kind of response might divide into two parts, either return the desired success response or parse the api error with the given Api Error model.
- **TokenAuthenticator** : Responsible for authenticating a token. It implements the Authenticator interface provided by OkHttp. Its purpose is to authenticate HTTP requests with an access token by implementing a token-based authentication scheme. 	It generally refreshes the access token if there are any issues with the token and after refresh it updates the prefs to further usage.

#### Preference
- **AppPreference Implementation** : The App Preference can be implemented with the interface with few methods and that preference can be implemented with other purposes like unit testing for test implementation.The AppPreferenceImpl class also provides implementations for the getUserPreference and getSettingsPreference methods of the AppPreference interface, which simply return the corresponding UserPreference and SettingsPreference objects that were provided to the constructor.
- **UserPreference** : User preference provides few exposed apis like the KEY_EMAIL, KEY_ACCESS_TOKEN, and KEY_REFRESH_TOKEN which can be used by implementing the preference interface. Overall, this interface provides a standardized way of accessing and manipulating user preferences related to email and authentication tokens.

# Auth Module
The auth module is an ideal example of an implementation of repository pattern in MVVM architecture.

The goal here is to achieve below capabilities:
-   **Single Source of Truth:** In information systems design, single source of truth (SSOT) is the practice of structuring information models and associated data schema such that every data element is mastered (or edited) in only one place.
-   **Code to Interface not Implementation:** Coding to interfaces is a technique to write classes based on an interface; interface that defines what the behavior of the object should be. It involves creating an interface first, defining its methods and then creating the actual class with the implementation.
-   **MVVM architecture:** Model — View — ViewModel (MVVM) is the industry-recognized software architecture pattern recommended by google.
-   **Unit Testing:** Smallest units of the application tested individually and independently.


Let’s go through the auth module and observe how it achieves above criteria.

-   **AuthViewModel:** All the authentication functionality is in this view-model as per MVVM            architecture pattern. It is decoupled from any UI components and any view can access             authentication functionality from here.
-   **AuthLocalDS:** All local/offline authentication functionalities are declared in this interface.     The architecture design expects another class to implement these functions and thus separates     authentication persisting logic.
-   **AuthLocalDSImpl:** This class implements the AuthLocalDS interface and encapsulates all            persistent logics. It is fully decoupled and thus fully mockable for unit testing.
-   **AuthRemoteDS:** All remote/online authentication functionalities are declared in this              interface. The architecture design expects another class to implement these functions and        thus    separates remote auth data fetching logic.
-   **AuthRemoteDSImpl:** This class implements the AuthRemoteDS interface and encapsulates all          remote    auth data fetching logic. It is fully decoupled and thus fully mockable for unit       testing.
-   **AuthRepository:** This repository has all local and remote auth related functionality declared.
-   **AuthRepositoryImpl:** This class implements AuthRepository and uses AuthLocalDS & AuthRemoteDS     where necessary but fully decoupled from AuthLocalDSImpl & AuthRemoteDSImpl. Therefore, it       can also be unit tested thoroughly.
-   **Auth Models:** AuthToken, EmailLoginRequest, LoginResponse, RefreshTokenResponse these models      are used for local & remote data structures.


If we observe closely,
-   The ‘email’ property can be updated only through UserPreference interface which is accessible     through AuthLocalDSImpl interface. This pattern of access point is true for all other            properties in the application and thus ***Single Source of Truth is implemented.***
-   Repository pattern is followed in every layer and thus ***‘Code to Interface not                 Implementation’     is followed.***
-   The module’s View, View-Model & Models are well-defined and functions in accordance to MVVM      architecture pattern and thus ***MVVM architecture is successfully designed.***
-   Every layer is decoupled through repository pattern and thus they ***can be tested through the      mechanism of Unit Testing.***



In Summary, by following the best architecture & design pattern guidelines, the auth module is a fully **unit-testable** and **scalable** application module. 
